![](https://img.shields.io/badge/20.0-python--telegram--bot-blue)
![](https://img.shields.io/badge/22.10.0-black-lightgrey)
# CEIMAFBOT

## C-3PO
El nombre C-3PO es de un personaje de star wars, un robot fabricado para encargase de las relaciones cibernéticas humanas.
## Descripción
El proyecto consta de un chatbot no interactivo que facilite el acceso a determinada información de la facultad. 
Primeramente está pensado para ingresantes que necesiten consultar dudas. Las cuales muchas veces son repetidas y no son contestadas a tiempo, ya que es un gran caudal de dudas el que ingresa a las redes del CEIMAF. Para ello este bot tratará de saldar las que pueda y las que la comunidad añada.

Funcionalidades:

```
start - Inicia el chatbot.
help - Brinda ayuda sobre los comandos.
mails - Brinda información sobre mails de secretarías y áreas.
comedor - Brinda link para reservar porción en el comedor.
horarios_de_cursado - Brinda información sobre los horarios de cursado de todas las materias.
ingresantes - Brinda información para ingresantes.
finales_viejos - Brinda link hacia el drive de finales y parciales viejos.
link_tree - Brinda link hacia el link tree del ceimaf.
preguntar - Brinda link hacia redes sociales del ceimaf.
```


Éste chatbot está basado en los siguientes códigos: 
- https://gitlab.com/rlyehlab/telegram-bot-priv-msgs
- https://github.com/sofide/telegram_bot_talk
- https://github.com/bormolina/templateborbot


## Instalación
1. Clonar el repositorio:

    > git clone link-to-this-repo

    Para este proyecto se recomienda crear un entorno virtual para aislar las tecnologías usadas.

2. Create a virtual env

    > virtualenv -p /usr/bin/python3 chatbot_ceimaf

3. Activate this virtual env

    > source /Path/to/venv/chatbot_ceimaf/bin/activate

4. Instalar dependencias 

    > pip install -r requirements.txt

5. Ejecutar 

    > python3 main.py

## Uso
El chatbot recibe comandos o palabras clave, pueden ser consultados los comandos ejecutando el comando ```/help```.
El chatbot puede mostrarnos mails útiles o horarios de cursado por ejemplo ejecutando lo siguiente:

```
    { user send }
+ /horarios_de_cursado
    { chatbot respond }
- [horarios_de_curasdo]
```

## Soporte
Para obtener más información sobre cómo colaborar, o para reportar un problema puede hacerlo mediante gitlab, o comunicandose con alguna red social del CEIMAF tales como:

- IG: @famaf.ceimaf
- Mail: famaf.ceimaf@gmail.com 


## Roadmap
Varias ideas o features por implementar pueden ser tales cómo:

1- Que las consultas sean específicas por carrera, o hasta consultar específicamente materias.

2- Que los datos sean tomados desde FaMAF y no hardcodeados.

3- Añadir algún tipo de machine learning para hacer un chatbot más interactivo y con contexto que pueda ayudar a quien no conoce precisamente lo que quiere preguntar. [Proyecto similar](https://github.com/sarang0909/faq_chatbot).

4- Documentación.

5- Testing.

6- Vincular éste chatbot con otras redes sociales.

7- Agregar Storys o ConversationHandlers para hacerlo más interactivo. [Ej](https://docs.python-telegram-bot.org/en/stable/examples.conversationbot.html).

8- Que el KeyboardMarkup no se muestre, tiene una propiedad booleana que por default viene seteada en falso, pero en algunos chats se muestra igual.

## License
For open source projects, say how it is licensed.

## Información utilizada

[Diapositivas de los CCU](https://drive.google.com/drive/folders/1qqB4BqUqmjfSMVhRqzrnaVGmcG-tcu3X)

[Web de FaMAF de ingresantes](http://www.famaf.unc.edu.ar/ingresantes/)

[Arquitectura del Bot](https://github.com/python-telegram-bot/python-telegram-bot/wiki/Architecture)

[Api de Telegram](https://core.telegram.org/bots/api)