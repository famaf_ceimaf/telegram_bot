"""
En éste archivo la idea es que luego se reemplace lo hardcodeado por un parseo o extracción de datos directamente de la página de FaMAF.
"""

bot = """
Bienvenido a C-3PO, el bot del CEIMAF.
Consultá los comandos escribiendo /help
"""

comandos = """
Los comandos de C-3PO son:
    <b>start</b> --> Inicia el chatbot.
    <b>help</b> --> Brinda ayuda sobre los comandos.
    <b>mails</b> --> Brinda información sobre mails de secretarías y áreas.
    <b>comedor</b> --> Brinda link para reservar porción en el comedor.
    <b>horarios_de_cursado</b> --> Brinda información sobre los horarios de cursado de todas las materias.
    <b>ingresantes</b> --> Brinda información para ingresantes.
    <b>finales_viejos</b> --> Brinda link hacia el drive de finales y parciales viejos.
    <b>link_tree</b> --> Brinda link hacia el link tree del ceimaf.
    <b>preguntar</b> --> Brinda link hacia redes sociales del ceimaf.
"""

preinscripciones = """
Para participar del Curso de Nivelación es obligatorio PREINSCRIBIRSE. Las PREINSCRIPCIONES se realizarán entre el 14 de noviembre y el 13 de diciembre de 2022.

Esta Preinscripción es Obligatoria para todas las personas que aspiran a ingresar en FAMAF en 2023, pero si sos ingresante 2023 y ya hiciste la Preinscripción en los meses de junio y julio 2022, no debés realizarla nuevamente.

Preinscripción 2023:

Entre el 14 de noviembre y el 13 de diciembre de 2022, deberás enviar los siguientes requisitos al email: ingreso2023famaf@gmail.com

    ·Imagen del DNI frente y dorso (en un único archivo PDF - no se admitirán imágenes del DNI en varios archivos separados).
    ·Foto de rostro (en un solo archivo JPG o PDF)
    ·Enviar el FORMULARIO DE PREINSCRIPCIÓN COMPLETO: lo debes completar online (clic aquí). Para referencia puedes ver el siguiente videotutorial ( clic aquí).
    ·Escribir en el texto del email el turno de cursado elegido: TURNO MAÑANA (9 a 13 hs) o TURNO TARDE (14 a 18 hs).

POR FAVOR CARGAR CORRECTAMENTE EL NOMBRE Y APELLIDO TAL CUAL FIGURAN EN EL DNI.

Si ya tenés usuario del sistema Guaraní por ser estudiante de la UNC, entonces no hace falta que envíes el Formulario, PERO debes ingresar en tu Guaraní > Trámites > Preinscripción a Propuestas > FAMAF > ELEGIR LA CARRERA DE FAMAF QUE DESEAS CURSAR. Si el sistema te pide generar un turno, obviá ese ítem. Avisar en el email que ya hiciste la Preinscripción a través de tu Guaraní.


A los fines de que la preinscripción se haga de manera correcta, Despacho de Alumnos/as responderá cada email y se te avisará si falta algo. Antes de efectuar cualquier reclamo, esperar el email de confirmación de recepción.
"""

curso_nivelacion = """
¿Cuándo?

El Curso de Nivelación se dictará desde el 30 de enero al 28 de febrero de 2023.

¿Modalidad?

Se ofrece solamente bajo la modalidad presencial. Debe tenerse en cuenta que las carreras de grado que ofrece la FAMAF son todas de dictado presencial.

¿Horarios?

El/la estudiante podrá optar entre cursar de lunes a viernes de 9 a 13 o de 14 a 18 horas.

¿Requisitos?

​Completar el formulario de preinscripción (si nunca tuviste un usuario de guaraní), enviar imágenes del DNI y foto del rostro​, como se indica más abajo en la sección "¿Cómo inscribirte?"​.

Se aclara que para completar la Inscripción Definitiva (en el mes de febrero) se deberá contar con un certificado de estudios secundarios finalizados sin adeudar materias.

Obligatoriedad

El Curso de Nivelación es obligatorio para cursar cualquiera de las carreras de grado y pregrado de la FAMAF.
"""

inscripcion_definitiva = """

La INSCRIPCIÓN DEFINITIVA es la instancia en la cual el/la ingresante pasa a ser ESTUDIANTE DE LA UNIVERSIDAD, cumpliendo con el requisito de tener el Secundario Completo. Es necesaria para poder tener una ACTUACION ACADEMICA (tener una nota de aprobación o reprobación de una materia).

Se llevará a cabo en forma virtual entre el 1 y el 17 de febrero de 2023.

Se deberá enviar al email: ingreso2023famaf@gmail.com escaneo o foto en alta resolución del Comprobante de Estudios Secundarios completos (en el cual figure que "no adeuda ninguna materia") o del Certificado de Estudios Secundarios definitivo (Certificado Analítico).

En caso de no completar este paso, el alumno perderá la posibilidad de promocionar el Curso de Nivelación o rendir examen final del mismo. Sin embargo, podrá seguir cursando la/s materia/s en modalidad LIBRE, hasta tanto realice la Inscripción Definitiva, teniendo como fecha límite el 28 de abril de 2023. Pasada esa fecha se perderá la posibilidad de seguir cursando, y deberá realizar una nueva Preinscripción en el mes de junio de 2023, para ingresar el año próximo.

Por cualquier duda o consulta, escribir a ingreso@famaf.unc.edu.ar
"""

faq = """
https://www.famaf.unc.edu.ar/la-facultad/institucional/secretar%C3%ADas/secretaria-academica/preinscripcion/preguntas-frecuentes-sobre-ingresantes/"""

horarios_de_cursado = """
https://www.famaf.unc.edu.ar/la-facultad/institucional/secretar%C3%ADas/secretaria-academica/horarios-de-cursado/
"""

fuente = """
https://www.famaf.unc.edu.ar/la-facultad/institucional/secretar%C3%ADas/secretaria-academica/preinscripcion/
"""

mail_sa = """
academica@famaf.unc.edu.ar
"""

secretaria_academica = """
https://www.famaf.unc.edu.ar/la-facultad/institucional/secretar%C3%ADas/secretaria-academica/
"""

mail_sae = """
sae@famaf.unc.edu.ar
"""

sae = """
https://www.famaf.unc.edu.ar/la-facultad/institucional/secretar%C3%ADas/secretaria-de-asuntos-estudiantiles/
"""

mail_sg = """
sgeneral@famaf.unc.edu.ar
"""

secretaria_general = """
https://www.famaf.unc.edu.ar/la-facultad/institucional/secretar%C3%ADas/secretar%C3%ADa-general/
"""

mail_mde = """
mesadeentradas@famaf.unc.edu.ar
"""

mesa_de_entrada = """
https://www.famaf.unc.edu.ar/la-facultad/institucional/areas-y-departamentos/%C3%A1rea-administrativa/
"""

mail_dde = """
despachoestudiantes@famaf.unc.edu.ar
"""

despacho = """
https://www.famaf.unc.edu.ar/despachodealumnos/
"""

comedor = """
https://comedor.unc.edu.ar/
"""

parciales_y_finales = """
https://drive.google.com/drive/u/1/folders/1BxuZzwrdIok67AMM66eEBAbV3m1_RnoB
"""

link_tree = """
https://linktr.ee/guri_labisagra
"""

ceimaf = """
IG: https://www.instagram.com/famaf.ceimaf/
Mail: famaf.ceimaf@gmail.com 
Telegram: https://t.me/CEIMAF
"""

grupo_no_permitido = """
Para añadir éste bot en un grupo, pida permiso a sus administradores.
"""

sticker = "CAACAgEAAxkBAAIDVmPCsV_E-6dHyk53CidE6RnMkg_IAAJgAwACjvaWCsTQI9FkbXFULQQ"
