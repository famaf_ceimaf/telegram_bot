from local_settings import grupos_validos


def is_valid_group(chat) -> bool:
    res = False
    if chat.title in grupos_validos:
        res = True
    return res
