import logging
import commands
from local_settings import TOKEN
from telegram import __version__ as TG_VER
from telegram.ext import CommandHandler, MessageHandler, filters, Application

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 1):

    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )

# Enable logging

logging.basicConfig(
    filename="out.log",
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,
)

logger = logging.getLogger(__name__)


def main() -> None:
    tkn = TOKEN
    app = Application.builder().token(tkn).build()
    # Definición de nuestros comandos y mensajes que recibe nuestro bot
    app.add_handler(CommandHandler("start", commands.cmd_start))
    app.add_handler(CommandHandler("help", commands.cmd_help))
    app.add_handler(CommandHandler("mails", commands.cmd_mails))
    app.add_handler(CommandHandler("comedor", commands.cmd_lunch))
    app.add_handler(
        CommandHandler("horarios_de_cursado", commands.cmd_get_course_schedules)
    )
    app.add_handler(CommandHandler("ingresantes", commands.cmd_entrants))
    app.add_handler(CommandHandler("finales_viejos", commands.cmd_old_finals))
    app.add_handler(CommandHandler("link_tree", commands.cmd_link_tree))
    app.add_handler(CommandHandler("preguntar", commands.cmd_ask))
    app.add_handler(
        MessageHandler(filters.TEXT & ~filters.COMMAND, commands.message_handler)
    )
    # Iniciamos el bot
    # Ejecute el bot hasta que presione Ctrl-C o el proceso reciba SIGINT,
    # SIGTERM o SIGABRT.
    app.run_polling()


if __name__ == "__main__":
    print("El bot está listo para servir ...")
    main()
