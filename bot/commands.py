import logging
from telegram import ReplyKeyboardMarkup, Update
from telegram.ext import ContextTypes
import data
from functions import is_valid_group


async def cmd_start(update: Update, context) -> None:
    chat_id = update.message.chat_id
    type_chat = update.message.chat.type
    chat = update.message.chat
    msg = data.bot
    if not is_valid_group(chat) and not type_chat == "private":
        msg = data.grupo_no_permitido
        logging.warning(
            "Falló al iniciar el chat el usuario: %s:%s, enviando el mensaje: %s, en el grupo: %s",
            update.message.from_user.username,
            update.message.from_user.id,
            update.message.text,
            update.message.chat.title,
        )

    await context.bot.send_message(
        chat_id,
        msg,
    )
    await context.bot.send_sticker(
        chat_id,
        data.sticker,
    )


async def cmd_help(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    chat_id = update.message.chat_id
    type_chat = update.message.chat.type
    chat = update.message.chat
    msg = data.comandos
    if not is_valid_group(chat) and not type_chat == "private":
        msg = data.grupo_no_permitido
    await context.bot.send_message(chat_id, msg, parse_mode="HTML")


async def cmd_entrants(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    teclado = [
        ["Preinscripciones", "Curso Nivelación"],
        ["Inscripción Definitiva", "Preguntas Frecuentes"],
    ]

    teclado_telegram = ReplyKeyboardMarkup(
        teclado, one_time_keyboard=True, resize_keyboard=True, is_persistent=False
    )

    await update.message.reply_text("Elige una opción", reply_markup=teclado_telegram)


async def message_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    type_chat = update.message.chat.type
    chat = update.message.chat
    msg = update.message.text
    if not is_valid_group(chat) and not type_chat == "private":
        msg = data.grupo_no_permitido

    if msg == "Curso Nivelación":
        info = data.curso_nivelacion + data.fuente
        await update.message.reply_text(info)
    elif msg == "Preinscripciones":
        info = data.preinscripciones + data.fuente
        await update.message.reply_text(info)
    elif msg == "Inscripción Definitiva":
        info = data.inscripcion_definitiva + data.fuente
        await update.message.reply_text(info)
    elif msg == "Preguntas Frecuentes":
        info = data.faq + data.fuente
        await update.message.reply_text(info)
    elif msg == "Secretaría Académica":
        info = data.mail_sa + data.secretaria_academica
        await update.message.reply_text(info)
    elif msg == "Secretaría de Asuntos Estudiantiles(SAE)":
        info = data.mail_sae + data.sae
        await update.message.reply_text(info)
    elif msg == "Secretaría General":
        info = data.mail_sg + data.secretaria_general
        await update.message.reply_text(info)
    elif msg == "Mesa de entrada":
        info = data.mail_mde + data.mesa_de_entrada
        await update.message.reply_text(info)
    elif msg == "Despacho de Estudiantes":
        info = data.mail_dde + data.despacho
        await update.message.reply_text(info)
    else:
        await update.message.reply_text(msg)


async def cmd_mails(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    teclado = [
        ["Secretaría Académica", "Secretaría de Asuntos Estudiantiles(SAE)"],
        ["Secretaría General", "Mesa de entrada", "Despacho de Estudiantes"],
    ]

    teclado_telegram = ReplyKeyboardMarkup(
        teclado, one_time_keyboard=True, resize_keyboard=True, is_persistent=False
    )

    await update.message.reply_text("Elige una opción", reply_markup=teclado_telegram)


async def cmd_get_course_schedules(
    update: Update, context: ContextTypes.DEFAULT_TYPE
) -> None:
    type_chat = update.message.chat.type
    chat = update.message.chat
    msg = data.horarios_de_cursado
    if not is_valid_group(chat) and not type_chat == "private":
        msg = data.grupo_no_permitido

    await context.bot.send_message(update.message.chat_id, msg)


async def cmd_lunch(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    type_chat = update.message.chat.type
    chat = update.message.chat
    msg = data.comedor
    if not is_valid_group(chat) and not type_chat == "private":
        msg = data.grupo_no_permitido
    await context.bot.send_message(update.message.chat_id, msg)


async def cmd_old_finals(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    type_chat = update.message.chat.type
    chat = update.message.chat
    msg = data.parciales_y_finales
    if not is_valid_group(chat) and not type_chat == "private":
        msg = data.grupo_no_permitido
    await context.bot.send_message(update.message.chat_id, msg)


async def cmd_link_tree(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    type_chat = update.message.chat.type
    chat = update.message.chat
    msg = data.link_tree
    if not is_valid_group(chat) and not type_chat == "private":
        msg = data.grupo_no_permitido
    await context.bot.send_message(update.message.chat_id, msg)


async def cmd_ask(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    type_chat = update.message.chat.type
    chat = update.message.chat
    msg = data.ceimaf
    if not is_valid_group(chat) and not type_chat == "private":
        msg = data.grupo_no_permitido
    await context.bot.send_message(update.message.chat_id, msg)
